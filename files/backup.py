#!/usr/bin/env python3

import json
import fnmatch
import argparse
import os, os.path
import sys
import logging, logging.handlers
import subprocess

logger = logging.getLogger("backup")


def setup_logger(level=logging.INFO, component="backup"):
    """
    :params level: minimum logging level to emit into syslog
    :returns: logger instance
    """
    syslog = logging.handlers.SysLogHandler("/dev/log")

    formatter = logging.Formatter("%(name)s: %(message)s")

    syslog.setFormatter(formatter)
    logging.getLogger(component).addHandler(syslog)
    logging.getLogger(component).setLevel(level)
    return logger


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d", "--configuration_dir", help="Configuration directory", required=True
    )
    parser.add_argument("-b", "--backup_volume", help="ZFS base volume", required=True)
    parser.add_argument(
        "-i", "--backup_key", help="SSH key to use for access", required=True
    )
    parser.add_argument(
        "-m", "--mount_dir", help="ZFS mount base directory", required=True
    )
    parser.add_argument(
        "-l", "--limit", help="Limit backup to matching hosts", default=None
    )
    return parser.parse_args()


def backup_host(directory, backup_directory, ssh_key):
    host_fqdn = os.path.basename(directory)
    host_config = []
    for backup_file in os.scandir(directory):
        backup_config = json.loads(open(backup_file.path, "r").read())
        host_config.extend(backup_config)

    for dir_backup in host_config:
        ph = subprocess.Popen(
            [
                "/usr/bin/rsync",
                "-aRz",
                "--delete",
                "-e",
                f"ssh -i {ssh_key}",
                ":".join((host_fqdn, dir_backup)),
                backup_directory,
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        while ph.poll() is None:
            try:
                out, err = ph.communicate(timeout=5)
                if out:
                    for line in out.splitlines():
                        logger.info(line.decode('utf-8'))
                if err:
                    for line in err.splitlines():
                        logger.error(line.decode('utf-8'))
            except subprocess.TimeoutExpired:
                continue
            except TimeoutError:
                logger.error(
                    "Error backing up directory %s on host %s. Skipping host.",
                    dir_backup,
                    host_fqdn,
                )
                return False
    return True


def check_zfs(backup_volume, host):
    results = subprocess.run(
        ["/sbin/zfs", "list", "/".join((backup_volume, host))], capture_output=True
    )
    if results.returncode > 0:
        logger.error(
            "ZFS dataset %s/%s does not exist. Please create it before running this backup script.",
            backup_volume,
            host,
        )
        return False
    return True


def create_snapshot(backup_volume, host):
    """"""
    result = subprocess.run(
        [
            "zfs",
            "set",
            "com.sun:auto-snapshot:daily=true",
            "/".join((backup_volume, host)),
        ]
    )
    result = subprocess.run(
        [
            "zfs-auto-snapshot",
            "--default-exclude",
            "--quiet",
            "--syslog",
            "--label",
            "daily",
            "--keep=7",
            "/".join((backup_volume, host)),
        ]
    )
    if result.returncode > 1:
        return False
    result = subprocess.run(
        [
            "zfs",
            "set",
            "com.sun:auto-snapshot:daily=false",
            "/".join((backup_volume, host)),
        ]
    )
    return True


def main():
    args = parse_args()
    logger = setup_logger()
    for host in os.scandir(args.configuration_dir):
        if args.limit is not None and not fnmatch.fnmatch(host.name, args.limit):
            continue
        if not host.is_dir():
            continue
        logger.info("Starting backup on target_host=%s", host.name)
        check = check_zfs(args.backup_volume, host.name)
        if not check:
            logger.error("ZFS check unsuccessful, skipping target_host=%s", host.name)
            continue
        success = backup_host(
            host.path,
            "/".join((args.mount_dir, host.name)),
            args.backup_key,
        )
        if success:
            create_snapshot(args.backup_volume, host.name)
            logger.info("Finished backup on target_host=%s", host.name)
        else:
            logger.info("Failed backup on target_host=%s", host.name)
    return True


if __name__ == "__main__":
    sys.exit(not main())
