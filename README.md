# Ansible backup role

This role is to seutp backup operation while being composable.

Each role which has a data directory to backup and/or a backup directory to dump their database daily shall include the backup role as dependency.

On the backup host this role shall create a zfs volume in the backup pool named after the host to backup. Additionally a configuration partial shall be written to specify which directories to backup
